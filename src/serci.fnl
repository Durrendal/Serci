#!/usr/bin/fennel
;;Author: Will Sinatra <wpsinatra@gmail.com>
;; GPLv3.0 | v0.1

;;===== Libraries =====
(local lume (require "lume"))
(local http (require "socket.http"))
(local https (require "ssl.https"))
(local inspect (require "inspect"))
(local driver (require "luasql.sqlite3"))

;;===== Configuration =====
(var conftbl {"editor" "mg"
              "db_path" (.. (os.getenv "HOME") "/.serci.db")
              "tbl_def" "(id INTEGER PRIMARY KEY, title TEXT NOT NULL, info TEXT, data BLOB, date DATETIME DEFAULT CURRENT_TIMESTAMP)"
              "add_def" "('title', 'info')"})

(global dotfile (.. (os.getenv "HOME") "/.serci"))

(local env (driver.sqlite3))

;;===== Usage =====
(fn usage [flag]
  (if (= flag nil)
      (print ";;===== serci v0.1 =====
-c category              | Add a category
-d category              | Remove a category and all records in it
-i category title        | Add a record to a category
-d category title        | Remove a record from a category
-s seek_query            | Query categories, records, and titles")
      (= flag "-c")
      (= flag "-i")
      (print ";;===== -i category title [nil, file, ...] =====
What?:
Add some form of info to the database

Usage Brief:
serci -i recipes \"Chocolate Cookies\"
serci -i recipes \"Chocolate Cookies\" /path/to/file

Info:
If -i is passed 2 args, category and then title, it will load the editor in your .serci file and allow you to input any freely written text into the database. If -i is passed category title and then a path to a file, and the file exists at that path, then serci will read the file and insert it into the database. Finally serci allows free form cli insertion as well. So you could pass your entire text from the command line as well. This is mostly to enable the use of differing table patterns via the .serci file.

Redefining your tables (ie: changing it from title, text to item, cost, amount) would allow you to pass arguments up to the defined amount. Assuming you set the above table schema you could use this third methodology in this form ie: serci -i inventory 'lenovo t14' $2000 10")
      (= flag "-d")
      (print ";;===== -d category key=value =====
What?:
Delete a record from a category

Usage Brief:
serci -d recipes 'title = \"Grandma's Secret Cookie Recipe\"'
serci -d recipes 'date like 2021-01-02%'

Info:
Really we're just invoking delete from category where colum=val and passing the precise column = val argument to it. Which also means that whatever sql logic you need in your where query can be applied, you just need to be aware of your column names, by default this would be id, title, info, date.")
  (os.exit)))

;;===== Util =====
(fn util/split [val check]
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)

(fn util/ap [tbl]
  (for [i 1 (- (length tbl) 1)]
    i))

(fn util/exists? [f]
  (local f? (io.open f "r"))
  (if f?
      (f?:close)
      false))

(fn util/mitigate []
  (local valid_vars ["editor" "db_path" "tbl_def" "add_def"])
  (local valid_holds ["mg"
                      (.. (os.getenv "HOME") "/.serci.db")
                      "(id INTEGER PRIMARY KEY, title TEXT NOT NULL, info TEXT, data BLOB, date DATETIME DEFAULT CURRENT_TIMESTAMP)"
                      "('title', 'info')"])
  (var reset nil)
  (each [k v (pairs valid_vars)]
    (do
      (if (= (. conftbl v) nil)
          (do
            (tset conftbl v (. valid_holds k))
            (set reset true)))))
  (if (= reset true)
      (with-open [f (io.open dotfile "w")]
        (f:write (lume.serialize conftbl)))))

(fn util/conf []
  (if (util/exists? dotfile)
      (do
        (with-open [f (io.open dotfile "r")]
          (set conftbl (lume.deserialize (f:read "*a"))))
        (util/mitigate))
      (with-open [f (io.open dotfile "w")]
        (f:write (lume.serialize conftbl))))
  (global conn (env:connect (. conftbl "db_path"))))

(fn util/codes [response body tmpf]
  "Handle https response codes, provided a 200 OK write remote file into tmpfile and return path handler"
  (match response
    400 (do
          (print "Bad request encountered")
          (os.exit))
    404 (do
          (print "Resource not found, try a different source")
          (os.exit))
    301 (do
          (print "Resource moved")
          (os.exit))
    200 (do
          (with-open [f (io.open tmpf "w")]
            (f:write body))))
  tmpf)

(fn util/http [url]
  "Get remote file via http"
  (let
      [(body response) (http.request url)
       tmpf (os.tmpname)]
    (util/codes response body tmpf)))

(fn util/https [url]
  "Get remote file via https"
  (let
      [(body response) (https.request url)
       tmpf (os.tmpname)]
    (util/codes response body tmpf)))

(fn util/tmpfedit []
  (let
      [tmpf (os.tmpname)]
    (os.execute (.. conftbl.editor " " tmpf))
    tmpf))

(fn util/sanitize [strings]
  (-> strings
      (string.gsub "'" "''")))

;;(util/expand_url_tbl "https://www.lua.org/pil/" ["1.html" "1.1.html" "1.2.html" "1.3.html" "1.4.html"])
(fn util/expand_url_tbl [root [tbl]]
  (var current "")
  (local typ (. (util/split root "://") 1))
  (each [k v (pairs tbl)]
    (do
      (print current)
      (print (.. root current))
      (print typ)
      (set current v)))
  (if (= typ "http")
      (util/http (.. root current))
      (= typ "https")
      (util/https (.. root current))
      (do
        (print "Something's gone wrong with url parsing")
        (os.exit))))

;;Add an FTS virtual table that compromises of all values in the add_def, or have a virt_def too
;;===== Function =====
(fn table/add [category]
  (let
      [sql (.. "CREATE table " category " " (. conftbl "tbl_def"))]
    (conn:execute sql))
  (print (.. "New Category Added: " category)))

(fn table/del [category]
    (let
        [sql (.. "DROP table " category)]
      (conn:execute sql))
    (print (.. "Category Removed: " category)))

;;If passed a file, read file into database
;;If text is nil, load a tmpfile with editor and insert data this way
;;Make this a table iterator, so that you can auto map values to the add_def by simply passing them
;;This might require some kind of tokenizer, or a DSL for multi inserts? such that the editor is the interface, but you type out
;;thinkpad,10,$300 and it parses that into the vaules segment. This would be acceptable, though maybe not intuitive
(fn record/add/info [tbl]
  "Add a text record to a category"
  (if (= (length tbl) 2)
      (do
        (let
            [tmpf (util/tmpfedit)
             category (. tbl 1)
             title (. tbl 2)
             text (with-open [f (io.open tmpf "r")] (f:read "*a"))
             sql (.. "INSERT into " category " " (. conftbl "add_def") "values ('" title "','" (util/sanitize text) "')")]
          (conn:execute sql)
          (print (.. "Data captured in category: " category))))
      (= (util/exists? (. tbl 3)) true)
      (do
        (let
            [category (. tbl 1)
             title (. tbl 2)
             text (with-open [f (io.open (. tbl 3) "r")] (f:read "*a"))
             sql (.. "INSERT into " category " " (. conftbl "add_def") "values ('" title "','" (util/sanitize text) "')")]
          (conn:execute sql)
          (print (.. "Data captured in category: " category))))
      (do
        (var text "")
        (each [k v (pairs tbl)]
          (do
            (set text (.. text ", " "'" (util/sanitize v) "'"))))
        (let
            [category (. tbl 1)
             title (. tbl 2)
             sql (.. "INSERT into " category " " (. conftbl "add_def") " values ('" title text ")")]
          (print sql)
          (conn:execute sql)
          (print (.. "Data captured in category: " category))))))

(fn record/del [category field_and_val]
    (let
        [sql (.. "DELETE from table " category " WHERE " field_and_val)]
    (conn:execute sql))
  (print (.. "Record removed from category: " category)))
  
(fn record/seek [tbl]
  (if (= (. tbl 1) nil)
      (do
        (let
            [sql (conn:execute "SELECT name FROM sqlite_master WHERE type = 'table'")]
          (print "Available Categories:")
          (var row (sql:fetch {} "a"))
          (while row
            (print row.name)
            (set row (sql:fetch row "a")))
          (sql:close)))
      (do   
        (let
            [category (. tbl 1)
             title (if (= (. tbl 2) nil)
                       ""
                       (. tbl 2))
             sql (conn:execute (.. "SELECT title FROM " category " WHERE title like '%" title "%'"))]
          (print "Matching Titles:")
          (var row (sql:fetch {} "a"))
          (while row
            (print row.title)
            (set row (sql:fetch row "a")))
          (sql:close))
        (if (not (= (. tbl 2) nil))
            (let
                [category (. tbl 1)
                 title (if (= (. tbl 2) nil)
                           ""
                           (. tbl 2))
                 sql (conn:execute (.. "SELECT title FROM " category " WHERE info  like '%" title "%'"))]
              (print "Records with Context Matching Query:")
              (var row (sql:fetch {} "a"))
              (while row
                (print row.title)
                (set row (sql:fetch row "a")))
              (sql:close))))))

(fn record/ret [tbl]
  (print (inspect tbl))
  (let
      [category (. tbl 1)
       title (. tbl 2)
       sql (conn:execute (.. "SELECT info FROM " category " WHERE title = '" title "'"))]
    (print (.. "Record Info from: " title))
    (var row (sql:fetch {} "a"))
    (while row
      (print row.info)
      (set row (sql:fetch row "a")))
    (sql:close)))

;;big WIP, needs an arg parser to expand the table to vars
;;===== Main =====
(fn serci [arg]
  (util/conf)
  (let [[arg-tbl & rest] arg]
    (match [arg-tbl rest]
      ([arg1] ? (or (= arg1 "-h") (= arg1 "--help")))
      (usage (. rest 1))
      ([arg1] ? (or (= arg1 "-i") (= arg1 "--add-info")))
      (record/add/info rest)
      ;;arg-parser to be invoked on rest?
      ([arg1] ? (or (= arg1 "-c") (= arg1 "--add-category")))
      (table/add (. rest 1))
      ([arg1] ? (or (= arg1 "-d") (= arg1 "--delete")))
      (record/del (. rest 1))
      ([arg1] ? (or (= arg1 "-s") (= arg1 "--seek")))
      (record/seek rest)
      ([arg1] ? (or (= arg1 "-r") (= arg1 "--return")))
      (record/ret rest)
      _ (usage nil)))
  (conn:close))
  
(serci arg)
