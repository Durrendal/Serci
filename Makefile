#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	$(LUA_ROCKS) install lume --local
	$(LUA_ROCKS) install lsqlite3 --local
	$(LUA_ROCKS) install luasocket --local
	$(LUA_ROCKS) install luasec --local

compile-lua:
	$(MAKE) fetch-libraries
	fennel --compile --require-as-include src/serci.fnl > src/serci-lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/serci.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/serci.lua

install-lua:
	install ./src/serci.lua -D $(DESTDIR)/serci

compile-bin:
	$(MAKE) fetch-libraries
	cd ./src/ && fennel --compile-binary serci.fnl serci-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/serci-bin -D $(DESTDIR)/serci
